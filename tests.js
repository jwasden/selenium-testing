const
    addContext = require('mochawesome/addContext'),
    assert = require('assert'),
    chosenBrowser = process.argv[6],
    chromedriver = require("chromedriver"),
    dateformat = require('dateformat'),
    edgedriver = require("edgedriver"),
    fs = require('fs'),
    iedriver = require("iedriver"),
    selenium = require('selenium-webdriver'),
    test = require('selenium-webdriver/testing'),

    GoogleHomePage = require('./pages/google-home'),
    YahooHomePage = require('./pages/yahoo-home');

var now = new Date(),
    testTimeStamp = dateformat(now, "fullDate"),
    testFileName = chosenBrowser + "-test-" + testTimeStamp;
testFilePath = "reports/" + chosenBrowser + "-report, " + testTimeStamp;

// set environment variables to configure
// test report options, create screenshot folder
if (!fs.existsSync(testFilePath)) fs.mkdirSync(testFilePath);
if (!fs.existsSync(testFilePath + "/screenshots")) fs.mkdirSync(testFilePath + "/screenshots");
process.env.MOCHAWESOME_AUTOOPEN = true;
process.env.MOCHAWESOME_REPORTFILENAME = testFileName;
process.env.MOCHAWESOME_REPORTDIR = testFilePath;

// declare driver
const driver = new selenium.Builder()
    .forBrowser(chosenBrowser)
    .build();

// declare modeled page objects needed for testing
const googleHomePage = new GoogleHomePage(driver);
const yahooHomePage = new YahooHomePage(driver);

// declare tests
test.describe('Search for \'Programmer\' on Google', function () {

    test.it('should navigate to www.google.com', function () {

        googleHomePage.open()
            .then(() => {
                return googleHomePage.getPageUrl();
            })
            .then((pageUrl) => {
                assert.equal(pageUrl, "https://www.google.com/?gws_rd=ssl");
            })
            .then(() => {
                return driver.takeScreenshot();
            })
            .then((screenshot, err) => {
                fs.writeFile(screenshotname, screenshot, 'base64', function (err) {
                    if (!err === null) console.log(err);
                });
            });
        //
        // other test details
        //        
        let screenshotname = getScreenshotName(testFilePath, this.test.title);
        addContext(this, getScreenshotPath(this.test.title));
        disableTestTimeout.call(this);
    });

    test.it('should search for \'programmer\' on google', function () {

        driver.findElement(googleHomePage.locators.googleSearchBox)
            .then((searchbox) => {
                searchbox.sendKeys('programmer');
                searchbox.submit();

                return driver.findElement(googleHomePage.locators.googleSearchBox).getAttribute('value');
            })
            .then((searchQueryValue) => {
                assert.equal(searchQueryValue, 'programmer')
            })
            .then(() => {
                return driver.takeScreenshot();
            })
            .then((screenshot, err) => {
                fs.writeFile(screenshotname, screenshot, 'base64', function (err) {
                    if (!err === null) console.log(err);
                });
            });
        //
        // other test details
        //        
        let screenshotname = getScreenshotName(testFilePath, this.test.title);
        addContext(this, getScreenshotPath(this.test.title));
        disableTestTimeout.call(this);
    });
});

test.describe('Search for \'Wikipedia\' on Yahoo', function () {

    test.it('should navigate to www.yahoo.com', function () {

        yahooHomePage.open()
            .then(() => {
                return yahooHomePage.getPageUrl();
            })
            .then((pageUrl) => {
                assert.equal(pageUrl, "https://www.yahoo.com/");
            })
            .then(() => {
                return driver.takeScreenshot();
            })
            .then((screenshot, err) => {
                fs.writeFile(screenshotname, screenshot, 'base64', function (err) {
                    if (!err === null) console.log(err);
                });
            });
        //
        // other test details
        //       
        let screenshotname = getScreenshotName(testFilePath, this.test.title);
        addContext(this, getScreenshotPath(this.test.title));
        disableTestTimeout.call(this);
    });

    test.it('should search for \'wikipedia\' on yahoo', function () {

        driver.findElement(yahooHomePage.locators.yahooSearchBox)
            .then((searchbox) => {
                searchbox.sendKeys('Wikipedia');
                searchbox.submit();

                return driver.findElement(yahooHomePage.locators.yahooSearchBox).getAttribute('value')
            })
            .then((searchQueryValue) => {
                assert.equal(searchQueryValue, 'Wikipedia')
            })
            .then(() => {
                return driver.takeScreenshot();
            })
            .then((image, err) => {
                fs.writeFile(screenshotname, image, 'base64', function (err) {
                    if (!err === null) console.log(err);
                });
            });
        //
        // other test details
        //        
        let screenshotname = getScreenshotName(testFilePath, this.test.title);
        addContext(this, getScreenshotPath(this.test.title));
        disableTestTimeout.call(this);

        //close broswer after last test
        driver.quit();
    });

});


//helper functions
function getScreenshotName(filepath, title) {
    return filepath + "/screenshots/" + title + "-screenshot.png";
}

function getScreenshotPath(title) {
    let path = "./screenshots/" + title + "-screenshot.png"
    return path;
}

function disableTestTimeout() {
    this.timeout(0)
}