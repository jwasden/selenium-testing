# Selenium-Testing #

Selenium-Testing is a project to quickstart functional browser testing projects powered by selenium webdriver. 
Automated, concurrent browser testing running from a single code base never tasted so good.

### What in the samhill is this for? ###

Have you ever wanted to have a user click through your app to make sure everything works, but can't bring yourself to make your mom do it everytime you update your code? 

Behold, the answer.

Here's how to see a sample of automated browser tests that run both ie and chrome from one code base.

```
git clone https://growingStronger@bitbucket.org/growingStronger/selenium-testing.git
npm run ftests
```

You can also run tests in one browser at a time.

```
npm run chrome
```

Test results are saved as gorgeous html reports in the reports folder. They also should open automagically in your favorite browser when they are done.

### How do I make this work for me? ###

1. Model pages of the app you want to test. (see examples in the pages folder.)
2. Get to modifying the promised-based code examples in tests.js using the [selenium js docs.](https://seleniumhq.github.io/selenium/docs/api/javascript/index.html)
3. Run your tests with the confidence a Greek God.
4. Read your reports like a Alexandrian librarian.
5. Act with the confidence of an imposter. (Sorry, you're on your own.)
