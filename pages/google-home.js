const By = require("selenium-webdriver").By;

class GoogleHomePage {
    constructor(driver) {
        this.driver = driver;
        
        //add common webpage elements by css selector here, to easily reference them in the test code
        this.locators = {
            googleSearchBox: By.name('q'),
            googleSearchButton: By.name('btnK')
        };
    }

    //add methods to call off of the page object, to open a new instance of the webpage
    open() {
        return this.driver.get("http://www.google.com");
    }

    getPageUrl() {
        return this.driver.getCurrentUrl();
    }
  
    performSeachFor(query) {
        this.driver.findElement(this.locators.googleSearchBox)
            .sendKeys(query)

        this.driver.findElement(this.locators.googleSearchBox)
            .submit()
    }

    searchResultsSearchBox() {
        return this.driver.findElement(googleHomePage.locators.googleSearchBox).getAttribute('value')
    }

}

module.exports = GoogleHomePage;