const By = require("selenium-webdriver").By;

class YahooHomePage {
    constructor(driver) {
        this.driver = driver;
        
        //add common webpage elements by css selector here, to easily reference them in the test code
        this.locators = {
            yahooSearchBox: By.name('p'),
            googleSearchButton: By.name('btnK')
        };
    }

    

    //add methods to call off of the page object, to open a new instance of the webpage
    open() {
        return this.driver.get("http://www.yahoo.com");
    }

    getPageUrl() {
        return this.driver.getCurrentUrl();
    }
  
    // declare abstractions of common tasks here
    performYahooSeach(query) {
        this.driver.findElement(this.locators.yahooSearchBox)
            .sendKeys(query)
        
        this.driver.findElement(this.locators.yahooSearchBox)
            .submit()
    }

}

module.exports = YahooHomePage;